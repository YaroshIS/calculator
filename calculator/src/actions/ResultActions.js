import { SET_RESULT_VISIBILITY } from '../constants/Result';

export function setResultVisibility(value){
    return {
        type: SET_RESULT_VISIBILITY,
        payload: value
    }
}