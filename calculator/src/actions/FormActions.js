import {SET_FORM_VISIBILITY, SET_NUMBER_B, SET_NUMBER_A, CLEAR_NUMBERS} from '../constants/Form';

export function setNumberA(value) {
    return {
        type: SET_NUMBER_A,
        payload: value
    }
}

export function setNumberB(value) {
    return {
        type: SET_NUMBER_B,
        payload: value
    }
}

export function clearNumbers() {
    return {
        type: CLEAR_NUMBERS,
        payload: null
    }
}

export function setFormVisibility(value) {
    return {
        type: SET_FORM_VISIBILITY,
        payload: value
    }
}