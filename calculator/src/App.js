import React, {Component} from 'react';
import './App.css';
import {connect} from 'react-redux';
import Form from './containers/Form';
import Result from './containers/Result';

class App extends Component {
    render() {
        const form = this.props.showForm ?
            (<Form />) :
            null;

        const result = this.props.showResult ?
            (<Result/>):
            null;

        return (
            <div className="app">
                {form}
                {result}
            </div>
        );
    }
}

function mapStateToProps(state){
    return {
        showForm: state.form.showForm,
        showResult: state.result.showResult,
        sum: state.result.sum
    }
}

export default connect(mapStateToProps)(App)