import React, {Component} from 'react'
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import './Form.css';

import * as formActions from '../actions/FormActions';
import * as resultActions from '../actions/ResultActions';

class Form extends Component {
    constructor() {
        super();

        this.onInputChange = this.onInputChange.bind(this);
        this.onResultBtnClick = this.onResultBtnClick.bind(this);
    }

    onInputChange(event) {
        const target = event.target;

        if(target.name === 'numberA'){
            this.props.formActions.setNumberA(target.value)
        }else {
            this.props.formActions.setNumberB(target.value);
        }
    }

    onResultBtnClick(event) {
        this.props.formActions.setFormVisibility(false);
        this.props.resultActions.setResultVisibility(true);
    }

    render() {
        return (
            <div className="modal">
                <div className="form-header">
                    <h2 className="form-message">Enter two values: </h2>
                </div>
                <input type="number" name="numberA" className="form-input-number"
                       onChange={this.onInputChange}/>
                <input type="number" name="numberB" className="form-input-number"
                       onChange={this.onInputChange}/>
                <button className="form-btn-submit" onClick={this.onResultBtnClick} disabled={!this.props.canSubmit}>
                    Result
                </button>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return state.form;
}

function mapDispatchToProps(dispatch) {
    return {
        formActions: bindActionCreators(formActions, dispatch),
        resultActions: bindActionCreators(resultActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Form)