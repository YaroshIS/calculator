import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import './Result.css';

import * as resultActions from '../actions/ResultActions';
import * as formActions from '../actions/FormActions';

class Result extends Component {
    constructor(){
        super();

        this.onClickExitBtn = this.onClickExitBtn.bind(this);
    }
    onClickExitBtn() {
        this.props.resultActions.setResultVisibility(false);
        this.props.formActions.clearNumbers();
        this.props.formActions.setFormVisibility(true);
    }

    render() {
        return (
            <div className="modal">
                <h2>Result: </h2>
                <span className="result-value">{this.props.sum}</span>
                <a href="#" onClick={this.onClickExitBtn}>again</a>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        sum: state.form.numberA + state.form.numberB
    }
}

function mapDispatchToProps(dispatch) {
    return {
        resultActions: bindActionCreators(resultActions, dispatch),
        formActions: bindActionCreators(formActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Result)