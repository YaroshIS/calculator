import { combineReducers } from 'redux';
import form from './form';
import result from './result';

export default combineReducers({
    form,
    result
})

