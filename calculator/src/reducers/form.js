import {SET_FORM_VISIBILITY, CLEAR_NUMBERS, SET_NUMBER_A, SET_NUMBER_B} from '../constants/Form';

const initialState = {
    numberA: null,
    numberB: null,

    canSubmit: false,
    showForm: true
};

export default function form(state = initialState, action) {
    var isNumeric = function(value){
        return !isNaN(parseFloat(value)) && isFinite(value);
    };

    switch (action.type) {
        case SET_NUMBER_A:
            return {...state,
                numberA: +action.payload,
                canSubmit: isNumeric(action.payload) && isNumeric(state.numberB)
            };
        case SET_NUMBER_B:
            return {...state,
                numberB: +action.payload,
                canSubmit: isNumeric(action.payload) && isNumeric(state.numberA)
            };
        case CLEAR_NUMBERS:
            return {...state,
                numberA: null,
                numberB: null,
                canSubmit: false,
            };
        case SET_FORM_VISIBILITY:
            return {...state, showForm: action.payload};
        default:
            return state;
    }
}