import {SET_RESULT_VISIBILITY} from '../constants/Result'

const initialState = {
    sum: null,

    showResult: false,
};

export default function result(state = initialState, action) {
    switch (action.type) {
        case SET_RESULT_VISIBILITY:
            return {...state, showResult: action.payload};
        default:
            return state;
    }
}